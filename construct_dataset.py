###
# File: construct_dataset.py
# Project:
# Created Date: Friday, April 9th 2021, 11:46:23 am
# Author: Stéphane Tetsing
# Email: stephane.tetsing@carepathtech.de
# -----
# Description: 
# -----
# Last Modified: Friday, 9th April 2021 11:46:24 am
# Modified By: Stéphane Tetsing, (stephane.tetsing@carepathtech.de)
# -----
# Copyright (c) 2021  Carepath Technologies GmbH
# 
# All Rights reserved
# -----
# HISTORY:
# Date      	By	Comments
# ----------	---	----------------------------------------------------------
###
import sys
sys.path.append('./')
import pandas as pd
from utils import load_data, features
from utils.features import chunk_row_NEW, extract_features
from tqdm import tqdm
import shutil
import torch
import os
import warnings
warnings.filterwarnings('ignore')

if os.path.exists('specs/'):
    try:
        shutil.rmtree('specs/')
    except OSError as e:
        print ("Error: %s - %s." % (e.filename, e.strerror))

os.makedirs('specs/')
os.makedirs('specs/train/')
os.makedirs('specs/test/')

CHUNK_LEN = 0.4
DATA_DIR = 'specs/'

# Read voxceleb data
sources = [
    "aac"
    ]

voxceleb_dir = '/voxceleb2/dev' 


def chunk_set(set_data):
    for idx in tqdm(range(len(set_data))):
        row = set_data.iloc[idx]
        fname = row.fileName
        patient_name = row.pers_id
        
        cough_chunks = chunk_row_NEW(row, chunk_len=CHUNK_LEN)
        
        if len(cough_chunks)>0:
            chunks_train, chunks_test = cough_chunks[:int(len(cough_chunks)/2)], cough_chunks[int(len(cough_chunks)/2):]
            extract_features(chunks_train, patient_name, DATA_DIR+'train/', fname)
            extract_features(chunks_test, patient_name, DATA_DIR+'test/', fname)



# load vox celeb data 
vx_data = load_data.get_cough_data_vx(sources=sources, voxceleb_dir=voxceleb_dir, n_speakers=10, n_dirs_per_speaker=5, n_files_per_dir_speakers=1)

chunks = chunk_set(vx_data)

