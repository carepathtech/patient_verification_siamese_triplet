###
# File: load_data.py
# Project:
# Created Date: Tuesday, October 6th 2020, 1:14:33 pm
# Author: Stéphane Tetsing
# Email: stephane.tetsing@carepathtech.de
# -----
# Description: This file contains different method for loading the data and structure it into 
# a large file for further processing. 
# -----
# Last Modified: Tuesday, 6th October 2020 1:14:33 pm
# Modified By: Stéphane Tetsing, (stephane.tetsing@carepathtech.de)
# -----
# Copyright (c) 2020  Carepath Technologies GmbH
# 
# All Rights reserved
# -----
# HISTORY:
# Date      	By	Comments
# ----------	---	----------------------------------------------------------
# 2020-10-06	ST	Initial implementation 
###


import sys
import numpy as np
import pandas as pd
from scipy.io import wavfile
import os
import pickle
import librosa
from tqdm import tqdm
tqdm.pandas()

BASE_DATA_DIR = '/Users/alessandroromualdi/code/patient_verification/siamese-triplet/data'

# AUDIO_PREFIX = "Audio/"
# LABEL_PREFIX = "Labels/"


def get_cough_data(sources):
    '''
    Iterates throughout allthe audio and label dirctory while collecting only the coughs related data

    Parameters
    ----------
    `sources`: list of str, contain data sources

    Returns
    -------
    A dataframe holding all the audio data alng with their respective labels
    '''
    audio_dir = ""
    #label_dir = ""
    data = []
    count = 0

    # pick a student 
    for name in sources:
        audio_dir = BASE_DATA_DIR + str('/') + name + str("/") +    X
        #label_dir = BASE_DATA_DIR + str('/') + name + str("/") + LABEL_PREFIX
        print('Processing: ', name)

        eq_label_folder = audio_dir.replace(AUDIO_PREFIX, LABEL_PREFIX)
        # go through all the audio and label files
        #print(os.listdir(audio_dir))

        for audio_fln in sorted(os.listdir(audio_dir)):
            if '.wav' not in audio_fln:
                continue

            count += 1
            # respective label file
            label_fln = audio_fln.replace('.wav', '.txt')
            try:
                if label_fln not in os.listdir(eq_label_folder):
                    #print('Label file for audio file', audio_fln, 'not found')
                    continue
            except Exception as e:
                print(e)
                continue

            label = pd.read_csv(eq_label_folder + str('/') + label_fln, sep="\t", header=None, names=['start', 'end', 'label'])

            df_cough = label[label.label == 'Cough']
            
            try:
                signal, rate = librosa.load(audio_dir + str('/') + audio_fln, sr=44100, mono=True, dtype=np.float32)
                if np.max(signal) > 1:
                    signal = signal / (2.0**(signal.itemsize*8-1))  # normalize signal between -1 and 1
            except Exception as ex:
                print(count, ex)
                continue
            data.append({'fileName':audio_fln, 'audio':signal, 'rate':rate, 'n_coughs': len(df_cough), 'labels':df_cough})
    print('Number of file read', count)

    return pd.DataFrame(data)


def get_cough_data_ONLY_AUDIO(source):

    audio_dir = ""
    data = []
    count = 0

    audio_dir = BASE_DATA_DIR + str('/') + source
    print('Processing: ', source)

    for audio_fln in sorted(os.listdir(audio_dir)):

        try:
            signal, rate = librosa.load(audio_dir + str('/') + audio_fln, sr=44100, mono=True, dtype=np.float32)
            if np.max(signal) > 1:
                signal = signal / (2.0**(signal.itemsize*8-1)) # normalize signal between -1 and 1
        except Exception as ex:
            print(count, ex)
            continue
        data.append({'fileName':audio_fln, 'audio':signal, 'rate':rate,})
    print('Number of file read', count)

    return pd.DataFrame(data)



def get_cough_data_vx(sources, voxceleb_dir, n_speakers=200, n_dirs_per_speaker=5, n_files_per_dir_speakers=15):
    '''
    Iterates throughout allthe audio and label dirctory while collecting only the coughs related data

    Parameters
    ----------
    `sources`: list of str, contain data sources

    Returns
    -------
    A dataframe holding all the audio data alng with their respective labels
    '''
    audio_dir = ""
    label_dir = ""
    data = []
    count = 0

    # pick a student 
    for name in sources:
        audio_dir = voxceleb_dir + str('/') + name + str('/')
        print('Processing: ', name)

        # list all the folder a student has label
        for audio_folder in tqdm(sorted(os.listdir(audio_dir)[:n_speakers])):
            if audio_folder.endswith('.DS_Store'):
                continue
            
            audio_folder = audio_dir + audio_folder

            # go through all the audio and label files
            for audio_sdir in os.listdir(audio_folder)[:n_dirs_per_speaker]:

                n_audio_folder = audio_folder + str('/') + audio_sdir
                print('new dir: ', audio_sdir)
                for audio_fln in sorted(os.listdir(n_audio_folder)[:n_files_per_dir_speakers]):

                    
                    if '.m4a' not in audio_fln:
                        continue

                    count += 1
                    
                    try:
                        #print(audio_fln)
                        signal, rate = librosa.load(n_audio_folder + str('/') + audio_fln, sr=44100, mono=True, dtype=np.float32)
                        slen = len(signal)/rate

                        df_cough = pd.DataFrame([{'start': 0, 'end': slen, 'label': 'cough'}])
                        if np.max(signal) > 1:
                            signal = signal / (2.0**(signal.itemsize*8-1))  # normalize signal between -1 and 1
                    except Exception as ex:
                        print('error in file nr:',count,n_audio_folder + str('/') + audio_fln , ex)
                        continue
                    #print(n_audio_folder)
                    data.append({'fileName':audio_fln, 'pers_id': n_audio_folder.split('/')[5], 'audio':signal, 'rate':rate, 'n_coughs': len(df_cough), 'labels':df_cough})
    print('Number of file read', count)

    return pd.DataFrame(data)
