

import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")

import sys
import numpy as np
import pickle as pkl 
import torch
import torchaudio
import torchvision
from PIL import Image
from joblib import dump
from scipy.io import wavfile
import torchaudio.transforms as T
import torch
# from torch_mfcc import STFT, FBANK, MFCC
import librosa

from tqdm import tqdm
tqdm.pandas()

sys.path.append('../src/')

import random
random.seed(123)


def extract_mfcc(
    values, data, target, rate=44100, n_mfcc=13, hop_length=512, window='hann'):

    mfcc = librosa.feature.mfcc(y=data, sr=rate, hop_length=hop_length, n_mfcc=n_mfcc, window=window)

    new_entry = {}
    new_entry["mfcc"] = mfcc
    new_entry["target"] = target
    values.append(new_entry)


def extract_features(chunks, label, output, fname):
    values = []
    for chunk in chunks:
        try:
            extract_mfcc(values, chunk, label)
        except RuntimeError:
            print('Skipped chunk for RuntimeError')
            pass

    # output chunk to file 
    for i, val in enumerate(values):
        outpath = output + '/' + fname + '_' + str(i) + ".pkl"
        with open(outpath, "wb") as handler:
            pkl.dump(val, handler, protocol=pkl.HIGHEST_PROTOCOL)


def getFrames(signal, samplingRate, frame_size=0.4, frame_overlap=0.2):
    '''
    Slides an audio signal in several equal partitioned frames with overlaping areas

    Parameters
    ----------
    `signal`: numpy array. The audio signal to be framed

    `samplingRate`: int. The sampling rate of the audio signal

    Returns
    -------
    The audio frames and their indices in the original audio signal

    '''
    # compute the frame length in samples
    frame_length, frame_step = int(np.ceil(
        frame_size * samplingRate)), int((frame_size - frame_overlap) * samplingRate)
    signal_length = len(signal)

    # number of frames, avoid short signal leading to negativity.
    # The last frame might not always be in the signal boundaries: --> np ceil
    n_frames = int(
        np.ceil(float(np.abs(signal_length - frame_length)) / frame_step))

    # new signal length padded
    new_signal_length = n_frames * frame_step + frame_length
    new_signal = np.append(np.squeeze(signal), np.zeros(
        (new_signal_length - signal_length)))

    # compute the indices
    indices = np.tile(np.arange(0, frame_length), (n_frames + 1, 1)) + \
        np.tile(np.arange(0, (n_frames + 1) * frame_step,
                          frame_step), (frame_length, 1)).T

    # slice the signal in frames
    frames = new_signal[indices.astype(np.int32, copy=False)]
    return frames, indices


def sub_chunk(audio, start, end, chunk_len, rate=44100):
    
    if (end - start) < chunk_len:
        raise ValueError(
            'Cannot chunk audio signal between `start` and `end`. (`end` - `start`) must be > `chunk_len`.')

    if (end - start) == chunk_len:
        return [audio[int(start * rate) : int(end * rate)]]

    else:
        chunks, _ = getFrames(audio, samplingRate=rate, frame_size=chunk_len)
        # parts = (end - start) // threshold
        # rest = (end - start) % threshold
        
        # chunks = []
        # for i in range(int(parts)):
        #     chunks.append( audio[ int( (start*rate + i*threshold*rate)): int( (start*rate + i*threshold*rate + threshold*rate)) ]) 
        
        # if rest:
        #     chunks.append( audio[ int(end*rate - threshold*rate) : int(end*rate) ]) 
            
        return chunks


# def sub_chunk(audio, start, end, chunk_len, rate=44100):
#     """
#     Takes an input array representing an audio signal, and reutrns chunks of duration `chunk_len`.
#     If the duraiton of the audio signal is equal  `chunk_len`, returns a list of single chunk.
#     If the duraiton of the audio signal is greater than a multiple of `chunk_len`, uses the modulus of
#     the ratio (`end` - `start`) / `chunk_len` to create an additional chunk from the last fragment.

#     """

#     if (end - start) < chunk_len:
#         raise ValueError(
#             'Cannot chunk audio signal between `start` and `end`. (`end` - `start`) must be > `chunk_len`.')


#     if (end - start) == chunk_len:
#         return [audio[int(start * rate) : int(end * rate)]]

#     else:
#         parts = (end - start) // chunk_len
#         rest = (end - start) % chunk_len
        
#         chunks = []
#         for i in range(int(parts)):
#             chunks.append(
#                 audio[ int( (start + i * chunk_len) * rate ): int( (start + i * chunk_len + chunk_len) * rate )]
#                 )
        
#         if rest:
#             chunks.append(
#                 audio[ int( (end - chunk_len) *rate ) : int(end * rate)])
            
#         return chunks


def center_chunk(start, end, left_margin, right_margin, chunk_len):
    
    left_budget = start - left_margin
    right_budget = right_margin - end

    if left_budget==0.0:
        nstart, nend = 0.0, chunk_len

    elif right_budget==0.0:
        nstart, nend = end - chunk_len, end

    else:

        if left_budget > right_budget:
            ratio = right_budget / left_budget
        else:
            ratio = left_budget / right_budget
            
        diff = chunk_len - (end - start)
        
        nstart = start - (diff * ratio) 
        nend = end + (diff * (1 - ratio) )
    
    return nstart, nend 
    

def chunk_row(row, chunk_len=0.4, rate=44100, balance=False):
    """
    Create audio audio containing `cough`and `non-cough` fragments
    of length `chunk_len`.

    """

     ## ToDo: keep only one chunk if fragment is not > chunk + 10% chunk

    audio = row.audio
    duration = 1.0 * len(audio) / rate
    locs = row.labels

    # oerder labels in ascending time
    locs = locs.sort_values(by=['start'])

    # get cough chunks  
    cough_chunks = []
    for i in range(len(locs)):
        loc = locs.iloc[i]

        # case I: cough length is longer or equal than chunk_len
        if loc.end - loc.start >= chunk_len:
            cough_chunks.extend(sub_chunk(audio, loc.start, loc.end, chunk_len))

        # case II: cough length is shorter than chunk_len
        else:
            # case: single cough
            if len(locs)==1:
                if duration >= chunk_len:
                    start, end = center_chunk(
                        loc.end, loc.start, left_margin=0.0, right_margin=duration, chunk_len=chunk_len)

                    cough_chunks.append( audio[ int(start * rate) : int(end * rate) ])
                
                else:
                    pass

            # case: multiple coughs
            elif len(locs)>1:
                if i==0:
                    left_margin = 0.0
                else:
                    left_margin = locs.iloc[i-1].end 

                if i==len(locs)-1:
                    right_margin = duration
                else:
                    right_margin = locs.iloc[i+1].start 
                    
                # if budget is enough to cover the chunk_len
                if (left_margin - right_margin >= chunk_len):

                    start, end = center_chunk(
                        loc.end, loc.start, left_margin, right_margin, chunk_len)
                    
                    cough_chunks.append( audio[int(start * rate) : int(end * rate)])

                else:
                    pass

    # populate non-cough slices
    noncough_chunks = []
    for i in range(len(locs)):

        if i==0:
            start = 0.0
        else:
            start = locs.iloc[i-1].end

        end = locs.iloc[i].start

        if end - start >= chunk_len:
            noncough_chunks.extend(sub_chunk(audio, start, end, chunk_len))

        # add rightmost part
        if (duration - locs.iloc[-1].end) >= chunk_len:
            noncough_chunks.extend(sub_chunk(audio, locs.iloc[-1].end, duration, chunk_len))


    # transform to numpy array
    cough_chunks = np.array(cough_chunks)
    noncough_chunks = np.array(noncough_chunks)

    # filter out empty chunks     
    cough_chunks = np.array([c for c in cough_chunks if len(c)>0])
    noncough_chunks = np.array([c for c in noncough_chunks if len(c)>0])

    if balance:
        random.shuffle(cough_chunks)
        random.shuffle(noncough_chunks)
        minlen = min(len(cough_chunks), len(noncough_chunks))
        cough_chunks = cough_chunks[:minlen]
        noncough_chunks = noncough_chunks[:minlen]

    return cough_chunks, noncough_chunks


def chunk_row_NEW(row, chunk_len=0.4, rate=44100, balance=False):

    audio = row.audio
    duration = 1.0 * len(audio) / rate

    return sub_chunk(audio, 0.0, duration, chunk_len)