from itertools import combinations

import numpy as np
import torch


def pdist(vectors):
    distance_matrix = -2 * vectors.mm(torch.t(vectors)) + vectors.pow(2).sum(dim=1).view(1, -1) + vectors.pow(2).sum(
        dim=1).view(-1, 1)
    return distance_matrix


class PairSelector:
    """
    Implementation should return indices of positive pairs and negative pairs that will be passed to compute
    Contrastive Loss
    return positive_pairs, negative_pairs
    """

    def __init__(self):
        pass

    def get_pairs(self, embeddings, labels):
        raise NotImplementedError


class AllPositivePairSelector(PairSelector):
    """
    Discards embeddings and generates all possible pairs given labels.
    If balance is True, negative pairs are a random sample to match the number of positive samples
    """
    def __init__(self, balance=True):
        super(AllPositivePairSelector, self).__init__()
        self.balance = balance

    def get_pairs(self, embeddings, labels):
        labels = labels.cpu().data.numpy()
        all_pairs = np.array(list(combinations(range(len(labels)), 2)))
        all_pairs = torch.LongTensor(all_pairs)
        positive_pairs = all_pairs[(labels[all_pairs[:, 0]] == labels[all_pairs[:, 1]]).nonzero()]
        negative_pairs = all_pairs[(labels[all_pairs[:, 0]] != labels[all_pairs[:, 1]]).nonzero()]
        if self.balance:
            negative_pairs = negative_pairs[torch.randperm(len(negative_pairs))[:len(positive_pairs)]]

        return positive_pairs, negative_pairs


class HardNegativePairSelector(PairSelector):
    """
    Creates all possible positive pairs. For negative pairs, pairs with smallest distance are taken into consideration,
    matching the number of positive pairs.
    """

    def __init__(self, cpu=True):
        super(HardNegativePairSelector, self).__init__()
        self.cpu = cpu

    def get_pairs(self, embeddings, labels):
        if self.cpu:
            embeddings = embeddings.cpu()
        distance_matrix = pdist(embeddings)

        labels = labels.cpu().data.numpy()
        all_pairs = np.array(list(combinations(range(len(labels)), 2)))
        all_pairs = torch.LongTensor(all_pairs)
        positive_pairs = all_pairs[(labels[all_pairs[:, 0]] == labels[all_pairs[:, 1]]).nonzero()]
        negative_pairs = all_pairs[(labels[all_pairs[:, 0]] != labels[all_pairs[:, 1]]).nonzero()]

        negative_distances = distance_matrix[negative_pairs[:, 0], negative_pairs[:, 1]]
        negative_distances = negative_distances.cpu().data.numpy()
        top_negatives = np.argpartition(negative_distances, len(positive_pairs))[:len(positive_pairs)]
        top_negative_pairs = negative_pairs[torch.LongTensor(top_negatives)]

        return positive_pairs, top_negative_pairs


class TripletSelector:
    """
    Implementation should return indices of anchors, positive and negative samples
    return np array of shape [N_triplets x 3]
    """

    def __init__(self):
        pass

    def get_triplets(self, embeddings, labels):
        raise NotImplementedError


class AllTripletSelector(TripletSelector):
    """
    Returns all possible triplets
    May be impractical in most cases
    """

    def __init__(self):
        super(AllTripletSelector, self).__init__()

    def get_triplets(self, embeddings, labels):
        labels = labels.cpu().data.numpy()
        triplets = []
        for label in set(labels):
            label_mask = (labels == label)
            label_indices = np.where(label_mask)[0]
            if len(label_indices) < 2:
                continue
            negative_indices = np.where(np.logical_not(label_mask))[0]
            anchor_positives = list(combinations(label_indices, 2))  # All anchor-positive pairs

            # Add all negatives for all positive pairs
            temp_triplets = [[anchor_positive[0], anchor_positive[1], neg_ind] for anchor_positive in anchor_positives
                             for neg_ind in negative_indices]
            triplets += temp_triplets

        return torch.LongTensor(np.array(triplets))


def hardest_negative(loss_values):
    hard_negative = np.argmax(loss_values)
    return hard_negative if loss_values[hard_negative] > 0 else None


def random_hard_negative(loss_values):
    hard_negatives = np.where(loss_values > 0)[0]
    return np.random.choice(hard_negatives) if len(hard_negatives) > 0 else None


def semihard_negative(loss_values, margin):
    semihard_negatives = np.where(np.logical_and(loss_values < margin, loss_values > 0))[0]
    return np.random.choice(semihard_negatives) if len(semihard_negatives) > 0 else None


class FunctionNegativeTripletSelector(TripletSelector):
    """
    For each positive pair, takes the hardest negative sample (with the greatest triplet loss value) to create a triplet
    Margin should match the margin used in triplet loss.
    negative_selection_fn should take array of loss_values for a given anchor-positive pair and all negative samples
    and return a negative index for that pair
    """

    def __init__(self, margin, negative_selection_fn, cpu=True):
        super(FunctionNegativeTripletSelector, self).__init__()
        self.cpu = cpu
        self.margin = margin
        self.negative_selection_fn = negative_selection_fn

    def get_triplets(self, embeddings, labels):
        if self.cpu:
            embeddings = embeddings.cpu()
        distance_matrix = pdist(embeddings)
        distance_matrix = distance_matrix.cpu()

        labels = labels.cpu().data.numpy()
        triplets = []

        for label in set(labels):
            label_mask = (labels == label)
            label_indices = np.where(label_mask)[0]
            if len(label_indices) < 2:
                continue
            negative_indices = np.where(np.logical_not(label_mask))[0]
            anchor_positives = list(combinations(label_indices, 2))  # All anchor-positive pairs
            anchor_positives = np.array(anchor_positives)

            ap_distances = distance_matrix[anchor_positives[:, 0], anchor_positives[:, 1]]
            for anchor_positive, ap_distance in zip(anchor_positives, ap_distances):
                loss_values = ap_distance - distance_matrix[torch.LongTensor(np.array([anchor_positive[0]])), torch.LongTensor(negative_indices)] + self.margin
                loss_values = loss_values.data.cpu().numpy()
                hard_negative = self.negative_selection_fn(loss_values)
                if hard_negative is not None:
                    hard_negative = negative_indices[hard_negative]
                    triplets.append([anchor_positive[0], anchor_positive[1], hard_negative])

        if len(triplets) == 0:
            triplets.append([anchor_positive[0], anchor_positive[1], negative_indices[0]])

        triplets = np.array(triplets)

        return torch.LongTensor(triplets)


def HardestNegativeTripletSelector(margin, cpu=False): return FunctionNegativeTripletSelector(margin=margin,
                                                                                 negative_selection_fn=hardest_negative,
                                                                                 cpu=cpu)


def RandomNegativeTripletSelector(margin, cpu=False): return FunctionNegativeTripletSelector(margin=margin,
                                                                                negative_selection_fn=random_hard_negative,
                                                                                cpu=cpu)


def SemihardNegativeTripletSelector(margin, cpu=False): return FunctionNegativeTripletSelector(margin=margin,
                                                                                  negative_selection_fn=lambda x: semihard_negative(x, margin),
                                                                                  cpu=cpu)



# ####

# import warnings
# warnings.filterwarnings("ignore", message="numpy.dtype size changed")
# warnings.filterwarnings("ignore", message="numpy.ufunc size changed")

# import sys
# import numpy as np
# import pickle as pkl 
# import torch
# import torchaudio
# import torchvision
# from PIL import Image
# from joblib import dump
# from tqdm import tqdm
# tqdm.pandas()

# import random
# random.seed(123)

# from scipy.io import wavfile
# import torchaudio.transforms as T


# def extract_mfcc(values, clip, target, rate=44100, win_length=None, hop_length=512, n_mels = 128, n_mfcc = 128):

#     mfcc_transform = T.MFCC(
#         sample_rate=rate,
#         n_mfcc=n_mfcc,
#         melkwargs={
#             'n_fft': self.n_fft, 'n_mels': n_mels, 'hop_length': hop_length})

#     new_entry = {}
#     new_entry["mfcc"] = mfcc_transform(data)
#     new_entry["target"] = target
#     values.append(new_entry)


# def extract_features(chunks, label, output):
#     values = []
#     for chunk in chunks:
#         try:
#             extract_mfcc(values, chunk, label)
#         except RuntimeError:
#             print('Skipped chunk for RuntimeError')
#             pass

#     # output chunk to file 
#     for i, val in enumerate(values):
#         outpath = output + label + '_' + str(i) + ".pkl"
#         with open(outpath, "wb") as handler:
#             pkl.dump(val, handler, protocol=pkl.HIGHEST_PROTOCOL)


# def sub_chunk(audio, start, end, chunk_len, rate=44100):
#     """
#     Takes an input array representing an audio signal, and reutrns chunks of duration `chunk_len`.
#     If the duraiton of the audio signal is equal  `chunk_len`, returns a list of single chunk.
#     If the duraiton of the audio signal is greater than a multiple of `chunk_len`, uses the modulus of
#     the ratio (`end` - `start`) / `chunk_len` to create an additional chunk from the last fragment.

#     """

#     if (end - start) < chunk_len:
#         raise ValueError(
#             'Cannot chunk audio signal between `start` and `end`. (`end` - `start`) must be > `chunk_len`.')


#     if (end - start) == chunk_len:
#         return [audio[int(start * rate) : int(end * rate)]]

#     else:
#         parts = (end - start) // chunk_len
#         rest = (end - start) % chunk_len
        
#         chunks = []
#         for i in range(int(parts)):
#             chunks.append(
#                 audio[ int( (start + i * chunk_len) * rate ): int( (start + i * chunk_len + chunk_len) * rate )]
#                 )
        
#         if rest:
#             chunks.append(
#                 audio[ int( (end - chunk_len) *rate ) : int(end * rate)])
            
#         return chunks


# def center_chunk(start, end, left_margin, right_margin, chunk_len):
    
#     left_budget = start - left_margin
#     right_budget = right_margin - end

#     if left_budget==0.0:
#         nstart, nend = 0.0, chunk_len

#     elif right_budget==0.0:
#         nstart, nend = end - chunk_len, end

#     else:

#         if left_budget > right_budget:
#             ratio = right_budget / left_budget
#         else:
#             ratio = left_budget / right_budget
            
#         diff = chunk_len - (end - start)
        
#         nstart = start - (diff * ratio) 
#         nend = end + (diff * (1 - ratio) )
    
#     return nstart, nend 
    

# def chunk_row(row, chunk_len=0.4, rate=44100, balance=False):
#     """
#     Create audio audio containing `cough`and `non-cough` fragments
#     of length `chunk_len`.

#     """

#      ## ToDo: keep only one chunk if fragment is not > chunk + 10% chunk

#     audio = row.audio
#     duration = 1.0 * len(audio) / rate
#     locs = row.labels

#     # oerder labels in ascending time
#     locs = locs.sort_values(by=['start'])

#     # get cough chunks  
#     cough_chunks = []
#     for i in range(len(locs)):
#         loc = locs.iloc[i]

#         # case I: cough length is longer or equal than chunk_len
#         if loc.end - loc.start >= chunk_len:
#             cough_chunks.extend(sub_chunk(audio, loc.start, loc.end, chunk_len))

#         # case II: cough length is shorter than chunk_len
#         else:
#             # case: single cough
#             if len(locs)==1:
#                 if duration >= chunk_len:
#                     start, end = center_chunk(
#                         loc.end, loc.start, left_margin=0.0, right_margin=duration, chunk_len=chunk_len)

#                     cough_chunks.append( audio[ int(start * rate) : int(end * rate) ])
                
#                 else:
#                     pass

#             # case: multiple coughs
#             elif len(locs)>1:
#                 if i==0:
#                     left_margin = 0.0
#                 else:
#                     left_margin = locs.iloc[i-1].end 

#                 if i==len(locs)-1:
#                     right_margin = duration
#                 else:
#                     right_margin = locs.iloc[i+1].start 
                    
#                 # if budget is enough to cover the chunk_len
#                 if (left_margin - right_margin >= chunk_len):

#                     start, end = center_chunk(
#                         loc.end, loc.start, left_margin, right_margin, chunk_len)
                    
#                     cough_chunks.append( audio[int(start * rate) : int(end * rate)])

#                 else:
#                     pass

#     # populate non-cough slices
#     noncough_chunks = []
#     for i in range(len(locs)):

#         if i==0:
#             start = 0.0
#         else:
#             start = locs.iloc[i-1].end

#         end = locs.iloc[i].start

#         if end - start >= chunk_len:
#             noncough_chunks.extend(sub_chunk(audio, start, end, chunk_len))

#         # add rightmost part
#         if (duration - locs.iloc[-1].end) >= chunk_len:
#             noncough_chunks.extend(sub_chunk(audio, locs.iloc[-1].end, duration, chunk_len))


#     # transform to numpy array
#     cough_chunks = np.array(cough_chunks)
#     noncough_chunks = np.array(noncough_chunks)

#     # filter out empty chunks     
#     cough_chunks = np.array([c for c in cough_chunks if len(c)>0])
#     noncough_chunks = np.array([c for c in noncough_chunks if len(c)>0])

#     if balance:
#         random.shuffle(cough_chunks)
#         random.shuffle(noncough_chunks)
#         minlen = min(len(cough_chunks), len(noncough_chunks))
#         cough_chunks = cough_chunks[:minlen]
#         noncough_chunks = noncough_chunks[:minlen]

#     return cough_chunks, noncough_chunks