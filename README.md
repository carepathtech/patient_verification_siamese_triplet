# README #

# Patient Verification Algorithm 

This repository implemenrts the code for training a patient verification algorithm based on a PyTorch implementation of siamese and triplet networks for learning embeddings.

Siamese and triplet networks are useful to learn mappings from image to a compact Euclidean space where distances correspond to a measure of similarity [1]. Embeddings trained in such way can be used as features vectors for classification or few-shot learning tasks.


# Installation

Python version: *Python3.6*

Requirements
```bash
pip3 install -r requirements.txt
```

# Code structure

- **datasets.py**
  - *SiameseDataSet* class - wrapper for a torch-like dataset, returning random positive and negative pairs
  - *TripletDataSet* class - wrapper for a torch-like dataset, returning random triplets (anchor, positive and negative)
  - *BalancedBatchSampler* class - BatchSampler for data loader, randomly chooses *n_classes* and *n_samples* from each class based on labels
  - *CoughDataSet* class - torch-like dataset to hold processed mfccs from cough data

- **networks.py**
  - *EmbeddingNet* - base network for encoding images into embedding vector
  - *ClassificationNet* - wrapper for an embedding network, adds a fully connected layer and log softmax for classification
  - *SiameseNet* - wrapper for an embedding network, processes pairs of inputs
  - *TripletNet* - wrapper for an embedding network, processes triplets of inputs
- **losses.py**
  - *ContrastiveLoss* - contrastive loss for pairs of embeddings and pair target (same/different)
  - *TripletLoss* - triplet loss for triplets of embeddings
  - *OnlineContrastiveLoss* - contrastive loss for a mini-batch of embeddings. Uses a *PairSelector* object to find positive and negative pairs within a mini-batch using ground truth class labels and computes contrastive loss for these pairs
  - *OnlineTripletLoss* - triplet loss for a mini-batch of embeddings. Uses a *TripletSelector* object to find triplets within a mini-batch using ground truth class labels and computes triplet loss
- **trainer.py**
  - *fit* - unified function for training a network with different number of inputs and different types of loss functions
- **metrics.py**
  - Sample metrics that can be used with *fit* function from *trainer.py*
- **utils.py**
  - *PairSelector* - abstract class defining objects generating pairs based on embeddings and ground truth class labels. Can be used with *OnlineContrastiveLoss*.
    - *AllPositivePairSelector, HardNegativePairSelector* - PairSelector implementations
  - *TripletSelector* - abstract class defining objects generating triplets based on embeddings and ground truth class labels. Can be used with *OnlineTripletLoss*.
    - *AllTripletSelector*, *HardestNegativeTripletSelector*, *RandomNegativeTripletSelector*, *SemihardNegativeTripletSelector* - TripletSelector implementations


# Dataset generation

...

# Create the datasets


## 3 - Data generator pipeline for training
In order to facilitate the training process thereby considerably reducing the memory requirements, the training features are precomputed and saved locally as well as validation features. The directory path are sved in the project configuration. See `config.json`
The following directories will contain the features:

- Training: **`./train_specs/train/`**
- Validation: **`./train_specs/val/`**
- Testing: **`./train_specs/val/`**

# Training 


# Evaluation and Performance Metrics


# Tests


# References

[2] Schroff, Florian, Dmitry Kalenichenko, and James Philbin. [Facenet: A unified embedding for face recognition and clustering.](https://arxiv.org/abs/1503.03832) CVPR 2015
